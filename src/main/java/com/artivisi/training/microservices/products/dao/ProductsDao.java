package com.artivisi.training.microservices.products.dao;

import com.artivisi.training.microservices.products.entity.Products;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductsDao extends PagingAndSortingRepository<Products, String> {
}
