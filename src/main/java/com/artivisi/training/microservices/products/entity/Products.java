package com.artivisi.training.microservices.products.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//import javax.persistence.IdClass;
//import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity @Data
public class Products {

    @Id @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;

    @NotEmpty @NotNull
    @Size(min = 3, max = 36)
    private String productcode;

    @NotEmpty @NotNull
    @Size(min = 2, max = 255)
    private String productname;
}
