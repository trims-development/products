package com.artivisi.training.microservices.products;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplikasiProdukApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiProdukApplication.class, args);
	}

}
