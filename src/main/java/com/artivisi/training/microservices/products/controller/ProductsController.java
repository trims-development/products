package com.artivisi.training.microservices.products.controller;

import com.artivisi.training.microservices.products.dao.ProductsDao;
import com.artivisi.training.microservices.products.entity.Products;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/products")
public class ProductsController {

    @Autowired private ProductsDao productsDao;

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveNewProducts(@RequestBody @Valid Products products) {
        productsDao.save(products);
    }

    @GetMapping("/")
    public Page<Products> findCustomer(Pageable page) {
        return productsDao.findAll(page);
    }
}
