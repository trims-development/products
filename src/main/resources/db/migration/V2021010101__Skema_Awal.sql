create table products (
  id varchar(36),
  productcode varchar(36) not null,
  productname varchar(255) not null,
  primary key (id),
  unique (productcode)
);
